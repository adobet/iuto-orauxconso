============
Enonce 3
============


Dictionnaire de champignons
---------------------------
Donnez les instructions permettant de construire le dictionnaire champignons suivant :

``{'amanite phalloide': (True, 'pyrénées orientales'), 'girolle': (False, 'Seine maritime'),``
``'cèpe': (False, 'le midi'), 'lactaire délicieux': (False, 'pyrénées orientales'),``
``'cortinaire': (True, 'région parisienne'), 'gyromitre': (True, 'massif central'),``
``'trompette des morts': (False, "cote d'or")}``

Les clefs sont des noms de champignons, les valeurs indiquent si le champignon est vénéneux ou pas ainsi que son habitat (région) de prédilection. 

* Ajouter à ce dictionnaire la morille qui n'est pas vénéneuse et que l'on trouve surtout en Seine maritime.
* Afficher la liste des valeurs du dictionnaire en utilisant la méthode ad hoc. 
* Écrire une fonction permettant de construire la liste des valeurs (sans utiliser la méthode).


Champignons et fonctions
------------------------
#. Définir une fonction qui retourne deux dictionnaires ``veneneux`` et ``nonVeneneux`` contenant respectivement les champignons vénéneux et leur habitat et les champignons non vénéneux et leur habitat. 
#. Définir une fonction retournant le dictionnaire de tous les champignons d'une région (habitat). Sous quelle forme le représenterez vous ? 

Champignons et types de formes
------------------------------
On dispose maintenant d'un dictionnaire indiquant pour un champignon son type ; les types sont au nombre de quatre : à plis, à lames, à tubes et à formes non classiques.

Par exemple : 
``{'cèpe': 'à tubes', 'morille': 'à formes non classiques', 'gyromitre': 'à formes non classiques',``
``'lactaire délicieux': 'à lames', 'cortinaire': 'à lames', 'amanite phalloide': 'à lames',``
``'trompette des morts': 'à plis', 'girolle': 'à plis'}``

#. Construire à l'aide d'une fonction un dictionnaire indiquant pour chaque type la liste des champignons concernés et le fait qu'ils soient ou non vénéneux. 
#. En utilisant la fonction précédente, définir le type qui dispose du plus grand nombre de champignons. 

.. raw:: latex

    \newpage
