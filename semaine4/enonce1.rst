============
Enonce 1
============

Dictionnaire de Capitales
-------------------------
Donnez les instructions permettant de construire le dictionnaire ``capitales`` suivant :

``{'Londres': 8630100, 'Pékin': 20693000, 'Tokyo': 13189000, 'Moscou': 11541000,
'Jarkarta': 10187595,``
``'Séoul': 10528774, 'Mexico': 8851080}``

Les clefs sont des noms de capitales, les valeurs sont les nombres d'habitants.

* Ajouter à ce dictionnaire la capitale Lima avec 8481415 habitants.
* Modifier la valeur du nombre d'habitants de la ville de Moscou, car l'ancien nombre date de 2012, par 15512000.
* Afficher la liste des clefs du dictionnaire en utilisant la méthode ad hoc. 
* Écrire une fonction permettant de construire la liste des clefs du dictionnaire (sans utiliser la méthode les fournissant).




Capitales et fonctions
----------------------  
* Définir une fonction retournant le nom de la capitale ayant le plus grand nombre d'habitants. 
* Définir une fonction retournant un dictionnaire des capitales ayant plus de 12 000 000 habitants.
* Écrire une fonction qui recherche si une capitale est dans le dictionnaire ``capitales``. Si celle-ci n'est pas présente la rajouter. Votre fonction devra retourner deux informations : un booléen indiquant si la capitale était présente ou non et le dictionnaire éventuellement modifié. 


Capitales et Continents
-----------------------  
On dispose maintenant d'un dictionnaire indiquant pour une capitale donnée dans quel continent elle se trouve. 
Par exemple : 
``{'Moscou': 'Europe', 'Bangkok': 'Asie', 'Tokyo': 'Asie', 'Lima': 'Amérique', 'Mexico': 'Amérique',``
``'Pékin': 'Asie', 'Séoul': 'Asie', 'Londres': 'Europe', 'Jarkarta': 'Asie'}``

#. Construire par fonction un dictionnaire avec le nombre de capitales par continent. 
#. Construire un nouveau dictionnaire avec, par continent, le nombre de capitales et la somme totale des habitants. Quelle forme prendra votre dictionnaire (donnez des exemples) ?   

.. raw:: latex

    \newpage
