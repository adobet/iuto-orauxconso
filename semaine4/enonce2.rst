============
Enonce 2
============



Dictionnaire d'animaux
----------------------
Donnez les instructions permettant de construire le dictionnaire ``animaux`` suivant :

``{'éléphant': 'Asie', 'écureuil': 'Europe', 'panda': 'Asie', 'hippopotame': 'Afrique',``
``'girafe': 'Afrique', 'iguane': 'Amérique', 'lion': 'Afrique'}``

Les clefs sont des noms d'animaux, les valeurs sont les continents d'habitation de ces animaux. 

* Ajouter à ce dictionnaire le couple mésange et Europe.
* Afficher la liste des couples (clef, valeur) du dictionnaire en utilisant la méthode ad hoc. 
* Écrire une fonction permettant de construire la liste de tels couples (sans utiliser la méthode ci-dessus).


Animaux et fonctions
--------------------

#. Définir une fonction retournant un dictionnaire avec le nombre d'animaux recensés par continent.

#. En utilisant la fonction précédente, déterminer le continent ayant le plus grand nombre d'animaux. 

Animaux et nombre de représentants
----------------------------------   

On dispose maintenant d'un dictionnaire indiquant pour un animal le nombre de représentants existant dans le continent concerné.

Par exemple : 

``{'éléphant': 250, 'écureuil': 2000000, 'mésange': 2580, 'panda': 500, 'hippopotame': 890,``
``'girafe': 2580, 'iguane': 1450, 'lion': 1000}``


#. Construire à l'aide d'une fonction un dictionnaire indiquant pour chaque continent la liste des animaux avec le nombre de leurs représentants.
   
#. Quel est le continent qui a le plus grand nombre de représentants (au total) ? Vous serez peut-être amenés à écrire deux fonctions.

.. raw:: latex

    \newpage
