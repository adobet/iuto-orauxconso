============
Enonce 2
============
Que fait cette fonction ?
--------------------------

::
   
 def fonc(liste) :
      bo = False
      p = None
      nb = 0
      for elem in liste :
         if p == elem :
            nb = nb + 1
         else :
            nb = 1
         p = elem 
      if nb >= 3 :
         bo = True
      return bo
   

* Testez cette fonction avec la liste ``[1, 1, 3, 7, 8, 8, 9]``
* puis avec la liste ``[1, 1, 3, 7, 8, 8, 8, 9]``. 
* Que se passe-t-il si la liste est vide ? 
* Donnez des noms significatifs pour chaque variable.
* Définissez ce que fait cette fonction. 
* Réécrivez la en utilisant une boucle avec indices, puis une boucle while.


Somme des éléments d'une liste
------------------------------

On vous demande de définir une fonction permettant de fournir la somme des éléments d'une liste d'entiers.

Quelle instruction permet de donner la somme des éléments de la liste ``[-2, 8, -3, 6, -12, 0, 8, 4]`` ? Quel est le résultat ? 

Que se passe-t-il si la liste est vide ?

#. boucle for sur les éléments
#. boucle for avec les indices
#. boucle while

Somme des éléments positifs d'une liste
---------------------------------------

Dans la liste  ``[12, 5, -3, 8, -12]`` la somme est 25.
Dans la liste  ``[-6, 7, -1, -6, 9]`` la somme est 9.

Que proposez vous pour les listes  ``[]`` et  ``[-6, -8, -3]`` ?

Proposez une fonction fournissant la somme des éléments positifs d'une liste d'entiers. 

#. boucle for sur les éléments
#. boucle for avec les indices
#. boucle while

Et si on continue
-----------------
On vous demande de proposer une fonction permettant de calculer la moyenne des éléments d'une liste d'entiers (exemples + les diverses versions).

On vous demande ensuite de définir une fonction calculant la moyenne des éléments positifs d'une liste d'entiers (exemples + diverses versions).

.. raw:: latex

    \newpage
