============
Enonce 1
============
Que fait cette fonction ?
--------------------------

::
   
 def fonc(liste) :
     l = 1
     lm = 0
     p = None
     for elem in liste :
         if p == elem :
             l = l + 1
         else :
             l = 1
         p = elem
         if l > lm :
             lm = l
     return lm


* Testez cette fonction avec la liste ``[1, 2, 2, 2, 3, 4, 6, 1]``,
* puis avec la liste ``[1, 2, 2, 3, 7, 8, 7, 7, 7]``. 
* Que se passe-t-il si la liste est vide ? 
* Donnez des noms significatifs pour chaque variable.
* Définissez ce que fait cette fonction. 
* Réécrivez la en utilisant une boucle avec indices, puis une boucle while.



Minimum d'une liste
-------------------

On vous demande de définir une fonction permettant de fournir le minimum d'une liste d'entiers.

Quelle instruction permet de donner le minimum de la liste ``[-2, 8, -3, 6, -12, 0, 8, 4]`` ? Quel est le résultat ? 

Que se passe-t-il si la liste est vide ?

#. boucle for sur les éléments
#. boucle for avec les indices
#. boucle while

Second Minimum
--------------
Dans la liste  ``[12, 5, -3, 8, -12]`` le second minimum est -3.
Dans la liste  ``[-6, 7, -1, -6, 9]`` le second minimum est -6.
Que proposez vous pour les listes  ``[]`` et  ``[-5]`` ?

Proposez une fonction fournissant le second minimum d'une liste. 

#. boucle for sur les éléments
#. boucle for avec les indices
#. boucle while

Et si on continue
-----------------
On désire connaître le nième minimum d'une liste.
En vous inspirant de l'exercice précédent, que proposez-vous ?
Utilisez la même démarche (exemples et toutes les versions de boucle).       

.. raw:: latex

    \newpage 
