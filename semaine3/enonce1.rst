============
Enonce 1
============

Représentation mémoire
----------------------
On vous donne les listes suivantes :  

* ``l1 = [1,[2, 3],4]``
* ``l2 = [6,7,[]]`` 
* ``l3 = [l1[1],l2]`` 
* Représentez les trois listes en mémoire.    
* Quel est l'affichage de ``print(l3)``

Soient les instructions suivantes :
  
* ``l3[1][1] = l2[2]`` 
* ``l2[2] = l1[1]``

Donnez les affichages correspondant à : 

* ``print(l1)``
* ``print(l2)``
* ``print(l3)`` 

Donnez la représentation mémoire des trois listes.  

Slices
------

A l'aide des résultats obtenus dans l'exercice précédent, donnez les affichages suivants

#. ``print(l1[1:])``
#. ``print(l2[1:2])`` 
#. ``print(l3[-2:])``
   
Mémoire et fonctions
--------------------
On vous donne les trois fonctions f1, f2, f3 suivantes :

::
   
 def f1(liste) :
    res = []
    for i in range(len(liste)) :
        if liste[i] %3 == 0 :
            res.append(liste[i] -1)
        else :
            res.append(liste[i])
    return res

 def f2(liste) :
    res = []
    for i in range(len(liste)) :
        if i % 2 == 0 :
            res.append(liste[i])
        else :
            res.append(liste[i]+1)
    return res
 
 def f3(liste) :
    res = 0
    for elem in liste :
        if elem %2 == 0:
            res += elem
        else :
            res -= elem
    return res

* Dessinez la représentation mémoire des divers appels de fonctions et indiquez ce qu'affiche ``print(f3(f2(f1(l))))``, à l'aide de la liste ``l = [1, 2, 6, 4, 9, 12, 11]``

Produits, quantité et prix
--------------------------

On vous propose de représenter un produit sous la forme d'un triplet nom, quantité et prix unitaire. A partir d'une liste de produits (triplets) on vous propose de définir les fonctions suivantes :

#. le produit ayant la plus petite quantité dans la liste de produits, 
#. le nom du produit ayant le prix unitaire le plus bas, 
#. l'indice du produit ayant le plus petit prix unitaire, 
#. la valeur totale des produits de la liste, 
#. la liste des noms de produits avec leur prix total respectif, 
#. sauvegarder une liste de produits, 
#. restaurer une liste de produits,  
#. trier selon le prix unitaire une liste de produits, par ordre croissant (vous pouvez utiliser la fonction donnant l'indice du plus petit prix de produit et écrire une fonction qui supprime dans une liste un élément à un indice donné). 

.. raw:: latex

    \newpage
