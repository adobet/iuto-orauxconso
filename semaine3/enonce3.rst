============
Enonce 3
============


Représentation mémoire
----------------------
On vous donne les listes suivantes :  

* ``l1 = [1,2,[4,5]]``
* ``l2 = [6,7,8]`` 
* ``l3 = [l1[2],l2]``
    
* Représentez les trois listes en mémoire.    
* Quel est l'affichage de ``print(l3)``

Soient les instructions suivantes :

* ``l3[1][0] = 10`` 
* ``l1[2][0] = 12``

Donnez les affichages correspondant à : 

* ``print(l1)``
* ``print(l2)``
* ``print(l3)`` 

Donnez la représentation mémoire des trois listes.  

Slices
------

A l'aide des résultats obtenus dans l'exercice précédent, donnez les affichages suivants

#. ``print(l3[-1:])``
#. ``print(l1[-2:-1])`` 
#. ``print(l1[-2:])``
   
Mémoire et fonctions
--------------------
On vous donne les trois fonctions f1, f2, f3 suivantes :

::
   
 def f1(liste) :
    res = []
    for i in range(len(liste)) :
        if liste[i] %3 == 0 :
            res.append(liste[i]-1)
    return res
 
 def f2(liste) :
    res = []
    for i in range(len(liste)) :
        if liste[i] % 2 == 0 :
            res.append(liste[i]+1)
    return res
 
 def f3(liste) :
    res = 0
    for elem in liste :
        if elem %2 == 0:
            res += elem
        else :
            res -= elem
    return res

* Dessinez la représentation mémoire des divers appels de fonctions et indiquez ce qu'affiche ``print(f3(f2(f1(l))))``, à l'aide de la liste ``l = [1, 2, 6, 4, 9, 12, 11]``


Montagnes et altitude
---------------------  
On décide de représenter une montagne sous la forme d'un couple nom et altitude.
A partir d'une liste de montagnes (de couples), on désire définir les fonctions suivantes :

#. le nom de la plus haute montagne de la liste, 
#. l'indice de la plus haute montagne,  
#. la liste des noms de montagnes ayant une altitude > à 2000 m, 
#. sauvegarder une liste de montagnes,  
#. restaurer une liste de montagnes,  
#. trier selon l'altitude une liste de montagnes par ordre décroissant. On fournira uniquement les noms des montagnes triés (vous pouvez utiliser la fonction donnant l'indice de la plus haute montagne et écrire une fonction qui supprime dans une liste un élément à un indice donné). 

.. raw:: latex

    \newpage
