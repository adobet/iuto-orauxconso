.. Oraux consolidation documentation master file, created by
   sphinx-quickstart on Wed Oct 19 13:41:38 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Oraux de consolidation
========================

Contents:

.. toctree::
   :caption: Table of Contents
   :name: mastertoc
   :maxdepth: 2

   semaine1/index.rst
   semaine2/index.rst
   semaine3/index.rst
   semaine4/index.rst



