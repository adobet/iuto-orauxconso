============
Enonce 3
============
Que fait cette fonction ?
--------------------------

::
   
 def exo1(l, n) : 
    d = 0
    f = len(l) -1
    tr = False
    while not tr and d <= f : 
        m = (d+f) // 2
        if l[m] == n : 
            tr = True
        else : 
            if l[m] < n : 
                d = m + 1
            else : 
                f = m - 1
    return tr 
    
* Testez cette fonction avec la liste ``[1, 4, 7, 8, 15]`` et 9,
* puis avec la liste ``[1, 4, 5, 7, 8, 11, 12, 15]`` et 8. 
* Que se passe-t-il si la liste est vide ? 
* Définissez ce que fait cette fonction. 
* Proposez un invariant de boucle.

Extraire les mots d'une phrase
------------------------------

On vous demande d'écrire une fonction qui permet d'extraire tous les mots d'une phrase pour les sauvegarder dans une liste. Les mots ne seront séparés que par un ou plusieurs blancs. Ils ne contiennent que des minuscules. 

#. Donnez des exemples.
#. Donnez l'invariant de boucle. 

   
Extraire les nombres d'une phrase
---------------------------------
En utilisant la fonction précédente, définissez une fonction qui permet d'extraire les nombres d'une phrase pour les stocker dans une liste. Donnez des exemples et l'invariant de boucle.



Extraire les mots non nombres d'une phrase
------------------------------------------
En vous aidant de ce qui a été fait précédemment, écrivez une fonction qui extrait d'une phrase les mots qui ne sont pas des nombres pour les stocker dans une liste.

La phrase ``bonjour nous sommes le 12 avril 1945`` doit retourner la liste ``["bonjour", "nous", "sommes", "le", "avril"]``. 

Bien parenthésée
----------------
Définir une fonction qui détermine si une chaîne contenant éventuellement des parenthèses est correctement parenthésée. Donnez des exemples, utilisez une boucle while dont vous donnerez l'invariant. 

Entre la première paire de parenthèses
--------------------------------------
Définir une fonction qui si une phrase est correctement parenthésée extrait ce qu'il y a entre la première paire de parenthèses.
Donnez des exemples et utilisez une boucle while.  
   
.. raw:: latex

   \newpage 
