============
Enonce 1
============
Que fait cette fonction ?
--------------------------

::
   
 def exo1(ph) : 
    n = 0
    b = True
    for e in ph : 
        if e != ' ' : 
            if b : 
                b = False
                n = n + 1
        else : 
            b = True
    return n
 
* Testez cette fonction avec le paramètre ``"bonjour les vacances sont finies"``,
* puis ``"nous sommes un jour de semaine"``. 
* Que se passe-t-il si la chaîne est vide ?
* Définissez ce que fait cette fonction. 
* Réécrivez la en utilisant une boucle avec indices, puis une boucle while.
  
Recherche dans une liste et dans une chaîne
-------------------------------------------

On vous demande d'écrire une fonction qui détermine l'indice de la première occurrence d'un nombre dans une liste.

#. Proposez des exemples et écrivez cette fonction à l'aide d'une boucle while.
#. Vous proposerez un invariant de boucle.
#. Qu'en est-il de la recherche de l'indice de la première occurrence d'une lettre dans une chaîne de caractère ?
#. Pouvez vous améliorer votre première fonction si la liste est triée et comment ?
   
Liste triée et tri de liste
---------------------------

On vous demande d'écrire une fonction qui détermine si une liste est triée par ordre croissant.

#. Proposez des exemples et écrivez cette fonction à l'aide d'une boucle while. 
#. Proposez un invariant de boucle. 
#. Que proposez vous pour savoir si une liste est triée (ordre croissant ou décroissant) ?
#. Comment modifier la première fonction afin qu'elle fournisse l'indice dans la liste de début de décroissance si cette liste n'est pas triée par ordre croissant?      
#. En utilisant la fonction ci-dessus écrivez une fonction permettant de trier une liste par ordre croissant. Proposez des exemples et écrivez cette fonction à l'aide d'une boucle while. Proposez également un invariant. 

.. raw:: latex

    \newpage
